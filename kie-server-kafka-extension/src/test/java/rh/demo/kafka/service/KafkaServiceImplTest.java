/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.redhat.demo.kafka.ejb;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.function.Consumer;

import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.server.KafkaConfig;
import kafka.server.KafkaServer;
import kafka.utils.MockTime;
import kafka.utils.TestUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.utils.Time;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import kafka.zk.EmbeddedZookeeper;
import org.I0Itec.zkclient.ZkClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;

import org.junit.*;
import org.junit.rules.TemporaryFolder;
import org.junit.runners.MethodSorters;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import rh.demo.kafka.service.KafkaServiceImpl;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KafkaServiceImplTest {



    @ClassRule
    public static TemporaryFolder KAFKA_LOGS = new TemporaryFolder();
    @ClassRule
    public static TemporaryFolder ZK_DATA = new TemporaryFolder();
    private static KafkaServer kafkaServer;
    private static EmbeddedZookeeper zkServer;
    private static KafkaServiceImpl client;

    private static final Properties PRODUCER_CONFIG = new Properties();
    static {
        PRODUCER_CONFIG.put("bootstrap.servers", "localhost:9092");
        PRODUCER_CONFIG.put("acks", "all");
        PRODUCER_CONFIG.put("retries", "0");
        PRODUCER_CONFIG.put("batch.size", "16384");
        PRODUCER_CONFIG.put("linger.ms", "1");
        PRODUCER_CONFIG.put("buffer.memory", "33554432");
        PRODUCER_CONFIG.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        PRODUCER_CONFIG.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    }

    KafkaProducer<String, String> producer = new KafkaProducer<>(PRODUCER_CONFIG);


    @BeforeClass
    public static void startKafka() {

        File kafkaLogs = KAFKA_LOGS.getRoot();

        // setup Zookeeper
        zkServer = new EmbeddedZookeeper();
        String zkConnect = "localhost" + ":" + zkServer.port();
        ZkClient zkClient = new ZkClient(zkConnect, 30000, 30000, ZKStringSerializer$.MODULE$);
        ZkUtils zkUtils = ZkUtils.apply(zkClient, false);

        // setup Broker
        Properties brokerProps = new Properties();
        brokerProps.setProperty("zookeeper.connect", zkConnect);
        brokerProps.setProperty("broker.id", "0");
        brokerProps.setProperty("log.dirs", kafkaLogs.getAbsolutePath());
        brokerProps.setProperty("listeners", "PLAINTEXT://" + "localhost" + ":" + "9092");
        brokerProps.setProperty("offsets.topic.replication.factor", "1");
        KafkaConfig config = new KafkaConfig(brokerProps);
        Time mock = new MockTime();
        kafkaServer = TestUtils.createServer(config, mock);

        // create topic
        AdminUtils.createTopic(zkUtils, "kafka.ejb.client", 1, 1, new Properties(), RackAwareMode.Disabled$.MODULE$);
        AdminUtils.createTopic(zkUtils, "kafka.ejb.client-A", 1, 1, new Properties(), RackAwareMode.Disabled$.MODULE$);
        AdminUtils.createTopic(zkUtils, "kafka.ejb.client-B", 1, 1, new Properties(), RackAwareMode.Disabled$.MODULE$);

        client = new KafkaServiceImpl();
        client.init();
    }

    @AfterClass
    public static void stopKafka() throws InterruptedException {
        kafkaServer.shutdown();
        zkServer.shutdown();
    }

    @Test
    public void a_produce_and_consume_messages() throws InterruptedException {
        UUID consumerId = UUID.randomUUID();
        UUID message = UUID.randomUUID();

        @SuppressWarnings("unchecked")
        Consumer<ConsumerRecord> topicConsumer = (Consumer<ConsumerRecord>) Mockito.mock(Consumer.class);

        client.registerTopicConsumer(consumerId.toString(), Collections.singletonList("kafka.ejb.client"), topicConsumer);
        // wait for the consumer to start
        Thread.sleep(3500);
        producer.send(new ProducerRecord("kafka.ejb.client", "key", "test-A" + message));
        // give the consumer time to see the event
        Thread.sleep(100);

        client.deregisterTopicConsumer(consumerId.toString());

        ArgumentCaptor<ConsumerRecord> consumerRecord = ArgumentCaptor.forClass(ConsumerRecord.class);
        Mockito.verify(topicConsumer).accept(consumerRecord.capture());

        Assert.assertEquals("key", consumerRecord.getValue().key());
        Assert.assertEquals("test-A" + message, consumerRecord.getValue().value());
    }

    @Test
    public void b_produce_and_consume_messages_on_two_topics() throws InterruptedException {
        UUID consumerA = UUID.randomUUID();
        UUID consumerB = UUID.randomUUID();

        UUID messageA = UUID.randomUUID();
        UUID messageB = UUID.randomUUID();

        @SuppressWarnings("unchecked")
        Consumer<ConsumerRecord> topicConsumerA = Mockito.mock(Consumer.class);

        @SuppressWarnings("unchecked")
        Consumer<ConsumerRecord> topicConsumerB = Mockito.mock(Consumer.class);

        client.registerTopicConsumer(consumerA.toString(), Collections.singletonList("kafka.ejb.client-A"), topicConsumerA);
        // wait for the consumer to start
        Thread.sleep(3000);
        producer.send(new ProducerRecord("kafka.ejb.client-A", "key", "test-B" + messageA));
        // give the consumer time to see the event
        Thread.sleep(100);

        client.registerTopicConsumer(consumerB.toString(), Collections.singletonList("kafka.ejb.client-B"), topicConsumerB);

        // wait for the consumer to start
        Thread.sleep(3000);
        producer.send(new ProducerRecord("kafka.ejb.client-A", "key", "test-B" + messageA));
        producer.send(new ProducerRecord("kafka.ejb.client-B", "key", "test-B" + messageB));
        // give the consumer time to see the event
        Thread.sleep(100);

        client.deregisterTopicConsumer(consumerA.toString());
        client.deregisterTopicConsumer(consumerB.toString());

        ArgumentCaptor<ConsumerRecord> aRecords = ArgumentCaptor.forClass(ConsumerRecord.class);
        ArgumentCaptor<ConsumerRecord> bRecords = ArgumentCaptor.forClass(ConsumerRecord.class);

        Mockito.verify(topicConsumerA, Mockito.times(2)).accept(aRecords.capture());
        Mockito.verify(topicConsumerB).accept(bRecords.capture());

        List<ConsumerRecord> records = aRecords.getAllValues();
        Assert.assertEquals("key", records.get(0).key());
        Assert.assertEquals("test-B" + messageA, records.get(0).value());

        Assert.assertEquals("key", records.get(1).key());
        Assert.assertEquals("test-B" + messageA, records.get(1).value());

        ConsumerRecord record = bRecords.getValue();
        Assert.assertEquals("key", record.key());
        Assert.assertEquals("test-B" + messageB, record.value());
    }

    @Test
    public void c_register_and_deregister_consumer() throws InterruptedException {
        UUID consumerId = UUID.randomUUID();
        UUID message = UUID.randomUUID();

        @SuppressWarnings("unchecked")
        Consumer<ConsumerRecord> topicConsumer = Mockito.mock(Consumer.class);

        client.registerTopicConsumer(consumerId.toString(), Collections.singletonList("kafka.ejb.client"), topicConsumer);

        // wait for the consumer to start
        Thread.sleep(3000);
        producer.send(new ProducerRecord("kafka.ejb.client", "key", "test-C" + message));
        // give the consumer time to see the event
        Thread.sleep(100);

        client.deregisterTopicConsumer(consumerId.toString());

        // wait for the consumer to start
        Thread.sleep(3000);
        producer.send(new ProducerRecord("kafka.ejb.client", "key", "test-C" + message));
        // give the consumer time to see the event
        Thread.sleep(100);

        ArgumentCaptor<ConsumerRecord> consumerRecord = ArgumentCaptor.forClass(ConsumerRecord.class);
        Mockito.verify(topicConsumer, Mockito.times(1)).accept(consumerRecord.capture());

        ConsumerRecord record = consumerRecord.getValue();
        Assert.assertEquals("key", record.key());
        Assert.assertEquals("test-C" + message, record.value());
    }

    @Test
    public void d_shutdown(){
        client.shutdown();
    }

}
